<h2> formulario de Pilotos</h2>

<!-- errores para el formulario -->

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>

    @foreach($errors->all() as $error)
   <li> {{$error }} </li>
@endforeach

</ul>
    </div>

@endif


<!--**************************************-->

<div class="form-group">
<label for="Nombre"> Nombre </label>
<input type="text" class="form-control" name="Nombre" 
 value="{{ isset($empleados->Nombre)?$empleados->Nombre:old('Nombre') }}" id="Nombre"> <!-- old para que recuerde el campo rellenado si el ususario pone mal los campos -->


</div>

<div class="form-group">
<label for="PrimerApellido"> Primer Apellido </label>
<input type="text" class="form-control" name="PrimerApellido"
 value="{{ isset($empleados->PrimerApellido)?$empleados->PrimerApellido:old('PrimerApellido') }} " id="PrimerApellido">


</div>

<div class="form-group">
<label for="SegundoApellido"> Segundo Apellido </label>
<input type="text" class="form-control"name="SegundoApellido" 
value="{{ isset($empleados->SegundoApellido)?$empleados->SegundoApellido:old('SegundoApellido') }}" id="SegundoApellido">


</div>

<div class="form-group">
<label for="Correo"> Correo </label>
<input type="text" class="form-control" name="Correo" 
value="{{ isset($empleados->Correo)?$empleados->Correo:old('Correo') }}" id="Correo">


</div>

<div class="form-group">

<label for="Foto"></label>
@if(isset($empleados->Foto))
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$empleados->Foto }}" width="100"  alt="">
@endif
<input type="file" class="form-control" name="Foto"  value="" id="Foto">



</div>

<input  class="btn btn-success" type="submit" value="{{ $modo }} datos">

<a class="btn btn-primary" href="{{ url('empleados/') }}"> regresar </a>

