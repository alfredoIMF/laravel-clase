<h2>formulario de Pilotos</h2>

@extends('layouts.app')
@section('content')
<div class="container">

<div class="alert alert-success alert-dismissible" role="alert">
@if(Session::has('mensaje')) <!--ssi hay sesion llamada mensaje pues mostramos dicho mensaje  -->
{{ Session::get('mensaje') }}

@endif 


<button type="button" class="close" data-dismiss="alert" arial-label="Colse">
    <span aria-hidden="true">&times;</span>
</button>

</div>


<a href="{{ url('empleados/create') }}"  class="btn btn-success"> Registrar nuevo piloto </a>
<br>
<br>
<a href="{{ url('motos/create') }}"class="btn btn-success"> Registrar nueva moto </a>
<br>
<br>
<a href="{{ url('neumaticos/create') }}"class="btn btn-success"> Registrar nuevo neumatico </a>
<br>
<br>




<table class="table table-light">
   
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Primer Apellido</th>
            <th>Segundo Apellido</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>
   
    <tbody>
    @foreach($empleados1 as $empleados)
        <tr>
            <td>{{$empleados->id }}</td>


            <td>
            <img  class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$empleados->Foto }}"  width="200"  alt="">
            </td>


            <td>{{$empleados->Nombre }}</td>
            <td>{{$empleados->PrimerApellido }}</td>
            <td>{{$empleados->SegundoApellido }}</td>
            <td>{{$empleados->Correo }}</td>
            <td>

            <a href="{{ url('/empleados/'.$empleados->id.'/edit') }}">
            Editar | 
            </a>



            <form action="{{ url('/empleados/'.$empleados->id ) }}" class="d-inline" method="post"> <!-- colocar el boton al lado derecho -->
            @csrf
            {{ method_field('DELETE') }}
            
            <input  class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar?')" value="Borrar">
            
            
            
             </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection