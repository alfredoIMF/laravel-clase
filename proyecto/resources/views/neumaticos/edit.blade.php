@extends('layouts.app')
@section('content')
<div class="container">

<form action="{{ url('/neumaticos/'.$neumaticos->id ) }}" method="post" enctype="multipart/form-data">
@csrf
{{ method_field('PATCH') }}

@include('neumaticos.form',['modo'=>'Editar']);

</form>
</div>
@endsection