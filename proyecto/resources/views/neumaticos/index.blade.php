<h2>formulario de Neumaticos</h2>


@extends('layouts.app')

@section('content')
<div class="container">

@if(Session::has('mensaje')) <!--si hay sesion llamada mensaje pues mostramos dicho mensaje  -->
{{ Session::get('mensaje') }}

@endif

<a href="{{ url('empleados/create') }}"  class="btn btn-success"> Registrar nuevo piloto </a>
<br>
<br>
<a href="{{ url('motos/create') }}"class="btn btn-success"> Registrar nueva moto </a>
<br>
<br>
<a href="{{ url('neumaticos/create') }}"class="btn btn-success"> Registrar nuevo neumatico </a>
<br>
<br>


<table class="table table-light">
   
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Marca</th>
            <th>Tipo</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
   
    <tbody>
    @foreach($neumaticos1 as $neumaticos)
        <tr>
            <td>{{$neumaticos->id }}</td>

            <td>
            <img  class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$neumaticos->Foto }}"  width="200"  alt="">
            </td>


            <td>{{$neumaticos->Marca }}</td>
            <td>{{$neumaticos->Tipo }}</td>
            <td>{{$neumaticos->Estado }}</td>
            <td>
            
            <a href="{{ url('/neumaticos/'.$neumaticos->id.'/edit') }}" >
            Editar | 
            </a>
            
            <form action="{{ url('/neumaticos/'.$neumaticos->id ) }}" class="d-inline" method="post">
            @csrf
            {{ method_field('DELETE') }}
            <input class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar?')" value="Borrar">
            
             </td>
         </tr>
        @endforeach

    </tbody>
</table>
</div>
@endsection