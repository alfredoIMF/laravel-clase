<h2>formulario de Neumaticos</h2>

<!-- errores para el formulario -->

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>

    @foreach($errors->all() as $error)
   <li> {{$error }} </li>
@endforeach

</ul>
    </div>

@endif

<div class="form-group">
<label for="Marca"> Marca </label>
<input type="text" class="form-control" name="Marca"  value="{{ isset($neumaticos->Marca)?$neumaticos->Marca:old('Marca') }}" id="Marca">
 

</div>

<div class="form-group">
<label for="Tipo"> Tipo </label>
<input type="text" class="form-control" name="Tipo" value="{{ isset($neumaticos->Tipo)?$neumaticos->Tipo:old('Tipo') }}" id="Tipo">
 

</div>

<div class="form-group">
<label for="Estado"> Estado </label>
<input type="text" class="form-control" name="Estado" value="{{ isset($neumaticos->Estado)?$neumaticos->Estado:old('Estado') }}" id="Estado">
 

</div>

<div class="form-group">

<label for="Foto"></label>
@if(isset($neumaticos->Foto))
<img  class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$neumaticos->Foto }}"  width="100"  alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto">
 
</div>


<input class="btn btn-success" type="submit"value="{{ $modo }} datos">

<a class="btn btn-primary"  href="{{ url('neumaticos/') }}"> regresar </a>
 
