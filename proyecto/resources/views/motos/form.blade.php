<h2>formulario de Motos</h2>

<!-- errores para el formulario -->
@if(count($errors)>0)

<div class="alert alert-danger" role="alert">
<ul>

    @foreach($errors->all() as $error)
   <li> {{$error }} </li>
@endforeach

</ul>
    </div>

@endif


<!--**************************************-->

<div class="form-group">
<label for="Marca"> Marca </label>
<input type="text"  class="form-control" name="Marca"  value="{{ isset($motos->Marca)?$motos->Marca:old('Marca') }}" id="Marca">


</div>

<div class="form-group">
<label for="Patrocinador"> Patrocinador </label>
<input type="text" class="form-control" name="Patrocinador" value="{{ isset($motos->Patrocinador)?$motos->Patrocinador:old('Patrocinador') }}" id="Patrocinador">


</div>

<div class="form-group">
<label for="KvPotencia"> cv Potencia </label>
<input type="text" class="form-control" name="KvPotencia" value="{{ isset($motos->KvPotencia)?$motos->KvPotencia:old('KvPotencia') }}" id="KvPotencia">


</div>

<div class="form-group">
<label for="Peso"> Peso </label>
<input type="text" class="form-control" name="Peso" value="{{ isset($motos->Peso)?$motos->Peso:old('Peso') }}" id="Peso">


</div>

<div class="form-group">
<label for="Foto"></label>
@if(isset($motos->Foto))
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$motos->Foto }}"  width="100"  alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto">


</div>

<input class="btn btn-success" type="submit" value="{{ $modo }} datos">

<a class="btn btn-primary" href="{{ url('motos/') }}"> regresar </a>
