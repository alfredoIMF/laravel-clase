<h2>formulario de Motos</h2>


@extends('layouts.app')
@section('content')
<div class="container">

@if(Session::has('mensaje')) <!--ssi hay sesion llamada mensaje pues mostramos dicho mensaje  -->
{{ Session::get('mensaje') }}

@endif

<a href="{{ url('empleados/create') }}"  class="btn btn-success"> Registrar nuevo piloto </a>
<br>
<br>
<a href="{{ url('motos/create') }}"class="btn btn-success"> Registrar nueva moto </a>
<br>
<br>
<a href="{{ url('neumaticos/create') }}"class="btn btn-success"> Registrar nuevo neumatico </a>
<br>
<br>

<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Marca</th>
            <th>Patrocinador</th>
            <th>CV Potencia</th>
            <th>Peso</th>
            <th>Acciones</th>
        </tr>
    </thead>
   
    <tbody>
    <tbody>
    @foreach($motos1 as $motos)
        <tr>
            <td>{{$motos->id }}</td>


            <td>
            <img  class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$motos->Foto }}"  width="200"  alt="">
            </td>



            <td>{{$motos->Marca }}</td>
            <td>{{$motos->Patrocinador }}</td>
            <td>{{$motos->KvPotencia }}</td>
            <td>{{$motos->Peso }}</td>
            <td>
           
            <a href="{{ url('/motos/'.$motos->id.'/edit') }}">
            Editar | 
            </a>
            
            <form action="{{ url('/motos/'.$motos->id ) }}" class="d-inline" method="post">
            @csrf
            {{ method_field('DELETE') }}
            <input class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar?')" value="Borrar">
            
            
            
             </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection