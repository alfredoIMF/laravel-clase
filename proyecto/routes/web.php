<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpleadosController;
use App\Http\Controllers\MotosController;
use App\Http\Controllers\NeumaticosController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {   /*para que salga la pagina del login en el public*/
    return view('auth.login');
});



/*Route::get('/empleados', function (){
      return view('empleados.index');
});
Route::get('empleados/create', [EmpleadosController::class,'create']);
*/

Route::resource('empleados', EmpleadosController::class)->middleware('auth');

Route::resource('motos', MotosController::class)->middleware('auth');

Route::resource('neumaticos', NeumaticosController::class)->middleware('auth');

Auth::routes(['register'=>false,'reset'=>false]); /*quitar el registro y el recordar contraseña*/



Route::get('/home', [EmpleadosController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function (){

    Route::get('/', [EmpleadosController::class, 'index'])->name('home');
    
});