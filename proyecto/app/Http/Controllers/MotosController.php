<?php

namespace App\Http\Controllers;

use App\Models\Motos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class MotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datos['motos1']=Motos::paginate(5);


        return view('motos.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('motos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         /*VALIDACION CAMPOS FORMULARIO */

        $campos=[
            'Marca'=>'required|string|max:50',
            'Patrocinador'=>'required|string|max:50',
            'KvPotencia'=>'required|int|max:300',
            'Peso'=>'required|int|max:300',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];

         /*MENSAJES DE ERROR*/
         $mensaje=[

            'required'=>'El :attribute es requerido', /*attribute es el comodin*/
            'Foto.required'=>'Se necesita una foto'
         ];
        
        $this->validate($request,$campos,$mensaje);
        /********************************************/




        //$datosMotos = request()->all();
        $datosMotos = request()->except('_token');

        if($request->hasFile('Foto')){
            $datosMotos['Foto']=$request->file('Foto')->store('uploads','public');

        }

        Motos::insert($datosMotos);



        return redirect('motos')->with('mensaje','Moto agregada con exito');
        //return response()->json($datosMotos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Motos  $motos
     * @return \Illuminate\Http\Response
     */
    public function show(Motos $motos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Motos  $motos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $motos=Motos::findOrFail($id);
        return view('motos.edit',compact('motos') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Motos  $motos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $datosMotos = request()->except(['_token','_method']);
       

        if($request->hasFile('Foto')){
            
            $motos=Motos::findOrFail($id);
            
            Storage::delete('public/'.$motos->Foto);
           
           
            $datosMotos['Foto']=$request->file('Foto')->store('uploads','public');

        }
       
       
        Motos::where('id','=',$id)->update($datosMotos);
        $motos=Motos::findOrFail($id);
        //return view('motos.edit',compact('motos') );

        return redirect('empleados')->with('mensaje','Moto editada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Motos  $motos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $motos=Motos::findOrFail($id);

        if(Storage::delete('public/'.$motos->Foto)){

            Motos::destroy($id);

        }
    
        return redirect('motos')->with('mensaje','Moto eliminada');
        
    }
}
