<?php

namespace App\Http\Controllers;

use App\Models\Neumaticos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NeumaticosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datos['neumaticos1']=Neumaticos::paginate(5);


        return view('neumaticos.index',$datos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('neumaticos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         /*VALIDACION CAMPOS FORMULARIO */

         $campos=[
            'Marca'=>'required|string|max:50',
            'Tipo'=>'required|string|max:50',
            'Estado'=>'required|string|max:50',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];

         /*MENSAJES DE ERROR*/
         $mensaje=[

            'required'=>'El :attribute es requerido', /*attribute es el comodin*/
            'Foto.required'=>'Se necesita una foto'
         ];
        
        $this->validate($request,$campos,$mensaje);
        /********************************************/


        //$datosNeumaticos = request()->all();
        $datosNeumaticos = request()->except('_token');
        
        if($request->hasFile('Foto')){
            $datosNeumaticos['Foto']=$request->file('Foto')->store('uploads','public');

        }


        Neumaticos::insert($datosNeumaticos);

        return redirect('neumaticos')->with('mensaje','Neumatico agregado con exito');
        //return response()->json($datosNeumaticos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Neumaticos  $neumaticos
     * @return \Illuminate\Http\Response
     */
    public function show(Neumaticos $neumaticos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Neumaticos  $neumaticos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $neumaticos=Neumaticos::findOrFail($id);
        return view('neumaticos.edit',compact('neumaticos') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Neumaticos  $neumaticos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $datosNeumaticos = request()->except(['_token','_method']);


        if($request->hasFile('Foto')){
            $neumaticos=Neumaticos::findOrFail($id);
            
            Storage::delete('public/'.$neumaticos->Foto);


            $datosNeumaticos['Foto']=$request->file('Foto')->store('uploads','public');
        }


        Neumaticos::where('id','=',$id)->update($datosNeumaticos);
        $neumaticos=Neumaticos::findOrFail($id);
        //return view('neumaticos.edit',compact('neumaticos') );

        return redirect('empleados')->with('mensaje','Neumatico editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Neumaticos  $neumaticos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
      $neumaticos=Neumaticos::findOrFail($id);

        if(Storage::delete('public/'.$neumaticos->Foto)){

            Neumaticos::destroy($id);

        }
        
        return redirect('neumaticos')->with('mensaje','Neumatico borrado');
    }
}
